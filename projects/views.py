from django.shortcuts import render
from .models import Project, Task, Comment, Log
from .forms import CommentForm, LogForm, TaskEdit, ProjectAdd, TaskAdd, FilterLog
from django.shortcuts import redirect
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime
from django.core.mail import send_mail
from django.forms.models import model_to_dict
from django.http import HttpResponseNotFound
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.models import User


def all_projects(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    is_admin = False
    if request.user.groups.filter(name='admin').exists() or request.user.is_superuser:
        is_admin = True
        projects_list = Project.objects.all()
    else:
        projects_list = Project.objects.filter(users=request.user)

    page = request.GET.get('page', 1)
    paginator = Paginator(projects_list, 2)

    try:
        projects = paginator.page(page)
    except PageNotAnInteger:
        projects = paginator.page(1)
    except EmptyPage:
        projects = paginator.page(paginator.num_pages)
    return render(request, 'projects/all.html', {'projects': projects, 'is_admin': is_admin})


def one_project(request, id_url):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    is_admin = False
    if request.user.groups.filter(name='admin').exists() or request.user.is_superuser:
        project = Project.objects.get(id_url=id_url)
        is_admin = True
    else:
        project = Project.objects.get(users=request.user, id_url=id_url)

    tasks = Task.objects.filter(project_id=project.id)

    return render(request, 'projects/one.html', {'project': project, 'tasks': tasks, 'is_admin': is_admin})


def task(request, id_url, id_task):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    is_admin = False
    if request.user.groups.filter(name='admin').exists() or request.user.is_superuser:
        project = Project.objects.get(id_url=id_url)
        is_admin = True
    else:
        project = Project.objects.get(users=request.user, id_url=id_url)

    task = Task.objects.get(project_id=project.id, id=id_task)
    logs = Log.objects.filter(task_id=id_task)

    spend_time = 0
    for log in logs:
        spend_time += log.time

    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.task_id = id_task
            post.dataTime = datetime.datetime.now()
            post.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = CommentForm()
    comments = Comment.objects.filter(task_id=id_task).order_by('-dataTime')

    return render(request, 'projects/task.html',
                  {'task': task, 'comments': comments, 'form': form, 'spend_time': spend_time, 'is_admin': is_admin})


def task_log(request, id_url, id_task):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if request.user.groups.filter(name='admin').exists() or request.user.is_superuser:
        project = Project.objects.get(id_url=id_url)
    else:
        project = Project.objects.get(users=request.user, id_url=id_url)

    task = Task.objects.get(project_id=project.id, id=id_task)
    is_executor = task.executor == request.user

    if request.method == 'POST':
        form = LogForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.task_id = id_task
            post.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = LogForm()

    logs = Log.objects.filter(task_id=id_task)

    return render(request, 'projects/task_log.html',
                  {'task': task, 'logs': logs, 'form': form, 'is_executor': is_executor})


def task_edit(request, id_url, id_task):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if request.user.groups.filter(name='admin').exists() or request.user.is_superuser:
        project = Project.objects.get(id_url=id_url)
    else:
        project = Project.objects.get(users=request.user, id_url=id_url)

    task = Task.objects.get(project_id=project.id, id=id_task)

    executor_choices = []
    for executor in project.users.all():
        executor_choices.append((executor.id, executor))

    if request.method == 'POST':
        form = TaskEdit(request.POST, instance=task)
        if form.is_valid():
            post = form.save(commit=False)

            past = json.loads(request.session.get('task'))
            current = json.dumps(model_to_dict(task), cls=DjangoJSONEncoder)
            current = json.loads(current)
            diff = sorted(set(past.items()) ^ set(current.items()))

            if not diff:
                return HttpResponseRedirect(request.path_info)

            current_text = ''
            past_text = ''
            for val in diff:
                if val in current.items():
                    current_text += val[0] + ': ' + val[1] + '\n'
                else:
                    past_text += val[0] + ': ' + val[1] + '\n'

            subject = task.theme
            message = 'Current data:\n' + current_text + '\n\nPast data:\n' + past_text
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [task.executor.email, task.creator.email]
            send_mail(subject, message, email_from, recipient_list)

            post.save()

            return HttpResponseRedirect(request.path_info)
    else:
        form = TaskEdit(instance=task)
        request.session['task'] = json.dumps(model_to_dict(task), cls=DjangoJSONEncoder)

    form.fields['executor'].choices = executor_choices

    return render(request, 'projects/task_edit.html', {'task': task, 'form': form})


def add_project(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if not (request.user.groups.filter(name='admin').exists() or request.user.is_superuser):
        return HttpResponseNotFound()

    if request.method == 'POST':
        form = ProjectAdd(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = ProjectAdd()

    return render(request, 'projects/add.html', {'form': form})


def task_add(request, id_url):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if not (request.user.groups.filter(name='admin').exists() or request.user.is_superuser):
        return HttpResponseNotFound()

    project = Project.objects.get(id_url=id_url)

    executor_choices = []
    for executor in project.users.all():
        executor_choices.append((executor.id, executor))

    if request.method == 'POST':
        form = TaskAdd(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.project_id = project.id
            post.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = TaskAdd()

    form.fields['executor'].choices = executor_choices

    return render(request, 'projects/task_add.html', {'form': form, 'project': project})


def edit_project(request, id_url):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if not (request.user.groups.filter(name='admin').exists() or request.user.is_superuser):
        return HttpResponseNotFound()

    project = Project.objects.get(id_url=id_url)

    if request.method == 'POST':
        form = ProjectAdd(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = ProjectAdd(instance=project)

    return render(request, 'projects/edit.html', {'form': form, 'project': project})


def delete_project(request, id_url):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if not (request.user.groups.filter(name='admin').exists() or request.user.is_superuser):
        return HttpResponseNotFound()

    Project.objects.filter(id_url=id_url).delete()

    return HttpResponseRedirect('/projects/')


def delete_task(request, id_url, id_task):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    if not (request.user.groups.filter(name='admin').exists() or request.user.is_superuser):
        return HttpResponseNotFound()

    Task.objects.filter(id=id_task).delete()

    return HttpResponseRedirect('/projects/' + id_url)


def logs(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    is_admin = False
    if request.user.groups.filter(name='admin').exists() or request.user.is_superuser:
        is_admin = True
        logs = Log.objects.all()
        tasks = Task.objects.all()
    else:
        logs = Log.objects.filter(user_id=request.user.id)
        tasks = Task.objects.filter(executor=request.user.id)

    users = User.objects.filter(groups__name='user')

    task_choices = [(None, '-----')]
    for task in tasks:
        task_choices.append((task.id, task.theme))

    user_choices = [(None, '-----')]
    for user in users:
        user_choices.append((user.id, user.username))

    if request.method == 'POST':
        form = FilterLog(request.POST)
        form.fields['task'].choices = task_choices
        form.fields['user'].choices = user_choices
        if form.is_valid():
            selected_task = form.cleaned_data.get('task')
            selected_user = form.cleaned_data.get('user')
            if selected_task:
                logs = logs.filter(task_id=selected_task)
            if selected_user:
                logs = logs.filter(user_id=selected_user)
    else:
        form = FilterLog()
        form.fields['task'].choices = task_choices
        form.fields['user'].choices = user_choices

    return render(request, 'home.html', {'logs': logs, 'form': form, 'is_admin': is_admin})
