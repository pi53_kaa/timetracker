from django.urls import path
from . import views

urlpatterns = [
    path('', views.all_projects),
    path('add', views.add_project),
    path('<str:id_url>/', views.one_project),
    path('<str:id_url>/edit', views.edit_project),
    path('<str:id_url>/delete', views.delete_project),
    path('<str:id_url>/add', views.task_add),
    path('<str:id_url>/<int:id_task>', views.task),
    path('<str:id_url>/<int:id_task>/edit', views.task_edit),
    path('<str:id_url>/<int:id_task>/log', views.task_log),
    path('<str:id_url>/<int:id_task>/delete', views.delete_task),
]
