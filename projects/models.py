from django.db import models
from django.contrib.auth.models import User
from tinymce import models as tinymce_models
from django_currentuser.db.models import CurrentUserField
from django.core.validators import MinValueValidator


class Project(models.Model):
    name = models.CharField(max_length=100)
    description = tinymce_models.HTMLField()
    id_url = models.CharField(max_length=15, unique=True)
    users = models.ManyToManyField(User, )

    def __str__(self):
        return self.name


class Task(models.Model):
    BAG = 'BAG'
    FEATURE = 'FEATURE'
    TYPE_CHOICES = [
        (BAG, 'BAG'),
        (FEATURE, 'FEATURE'),
    ]

    NORMAL = 'NORMAL'
    HIGH = 'HIGH'
    URGENTLY = 'URGENTLY'
    PRIORITY_CHOICES = [
        (NORMAL, 'NORMAL'),
        (HIGH, 'HIGH'),
        (URGENTLY, 'URGENTLY'),
    ]

    theme = models.CharField(max_length=50)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    type = models.CharField(choices=TYPE_CHOICES, max_length=50)
    priority = models.CharField(choices=PRIORITY_CHOICES, max_length=50)
    estimated_time = models.PositiveIntegerField()
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    creator = CurrentUserField()
    executor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='executor')

    def __str__(self):
        return self.theme


class Comment(models.Model):
    text = models.TextField()
    dataTime = models.DateTimeField()
    user = CurrentUserField()
    task = models.ForeignKey(Task, on_delete=models.CASCADE)


class Log(models.Model):
    comment = models.TextField()
    time = models.IntegerField(validators=[MinValueValidator(1)])
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    user = CurrentUserField()
