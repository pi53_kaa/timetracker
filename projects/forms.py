from django import forms
from .models import Comment, Log, Task, Project
from tinymce.widgets import TinyMCE
from django.contrib.auth.models import User


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)


class LogForm(forms.ModelForm):
    time = forms.IntegerField(min_value=1)

    class Meta:
        model = Log
        fields = ('comment', 'time',)


class TaskEdit(forms.ModelForm):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = Task
        exclude = ('creator', 'project')


class ProjectAdd(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 50, 'rows': 20}))
    users = forms.ModelMultipleChoiceField(queryset=User.objects.filter(groups__name='user'))

    class Meta:
        model = Project
        exclude = ()


class TaskAdd(forms.ModelForm):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = Task
        exclude = ('creator', 'project')


class FilterLog(forms.Form):
    user = forms.ChoiceField(required=False)
    task = forms.ChoiceField(required=False)
