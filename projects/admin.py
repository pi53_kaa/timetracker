from django.contrib import admin
from .models import Project, Task


class TaskAdmin(admin.ModelAdmin):
    exclude = ('creator',)


admin.site.register(Project)
admin.site.register(Task, TaskAdmin)
