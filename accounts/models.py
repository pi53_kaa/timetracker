from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    position = models.CharField(max_length=50)
    birth_date = models.DateField()
    photo = models.ImageField(upload_to='user_img', blank=True)

    def __str__(self):
        return self.user.username
