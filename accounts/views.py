from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from .forms import EditProfileForm, ProfileForm


def profile(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    user_groups = []
    for group in request.user.groups.values_list('name', flat=True):
        user_groups.append(group)
    # if request.method == 'POST':
    #     form = EditProfileForm(request.POST, instance=request.user)
    #     custom_form = ProfileForm(request.POST, instance=request.user)
    #
    #     if form.is_valid():
    #         form.save()
    #         custom_form.save()
    #         return redirect('accounts/profile')
    #
    # else:
    #     form = EditProfileForm(instance=request.user)
    #     custom_form = ProfileForm(instance=request.user)
    return render(request, 'accounts/profile.html', {'user_groups': user_groups})
